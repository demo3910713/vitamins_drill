//  5. Sort items based on number of Vitamins they contain.

function sortByVitaminCount(a, b) {
  if(typeof a === "object" && typeof b === "object"){
  const vitaminsA = a.contains.split(", ").length;
  const vitaminsB = b.contains.split(", ").length;
  return vitaminsA - vitaminsB;
  }else{
    return "Data Insufficient"
  }
}
module.exports = sortByVitaminCount;
